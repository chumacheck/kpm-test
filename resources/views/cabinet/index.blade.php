@extends('layouts.main')

@section('title') @parent {{ $title }} @endsection

@section('content')
    <main class="form-signin home-form">
        <h1>Cabinet</h1>
        @if (count($contacts) < 1)
            <p>You have no contacts yet.</p>
            <p>Add them from main list on <a href="{{ route('home') }}">Home Page</a></p>
        @else
            <ul class="list-group">
                @foreach ($contacts as $contact)
                    <li class="list-group-item">
                        <p>{{ $contact->name }} - {{ $contact->phone }}</p>
                        <form method="GET" action="{{ route('deleteContact', ['id' => $contact->id]) }}">
                            @csrf
                            <button type="submit" class="btn btn-primary">Delete contact</button>
                        </form>
                    </li>
                @endforeach
            </ul>
            <a href="{{ route('home') }}">Add more contacts</a>
        @endif
    </main>
@endsection
