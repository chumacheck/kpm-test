
@include('layouts.header')

<div class="text-center main__body flex-column">
    @include('layouts.alerts')
    @yield('content')
</div>

@include('layouts.footer')

