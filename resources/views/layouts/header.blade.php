<!doctype html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="Igor Olkhin">
            <title>@section('title') КПМ - @show</title>
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
            <meta name="theme-color" content="#7952b3">
            <link href="/public/css/signin.css" rel="stylesheet">
        </head>
        <body >
        <div class="header">
            <div class="header__inner">
                <ul class="nav justify-content-end custom__nav">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{ route('home') }}">Home</a>
                    </li>
                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}">Logout</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cabinet') }}">My Contacts</a>
                        </li>
                    @endauth
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register.create') }}">Register/Log in</a>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
