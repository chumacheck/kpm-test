@extends('layouts.main')

@section('title') @parent {{ $title }} @endsection

@section('content')
    <main class="form-signin home-form">
        <h1>HOME</h1>
        @guest
            <h2>Register to View available list of contacts</h2>
            <ul class="list-group blur">
                @foreach ($contacts as $contact)
                    <li class="list-group-item">
                        <p>Имя Фамилия - {{ $contact->phone }}</p>
                    </li>
                @endforeach
            </ul>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="blur__svg">
                <filter id="blur">
                    <feGaussianBlur stdDeviation="2" />
                </filter>
            </svg>
        @endguest
        @auth
        <ul class="list-group">
            @foreach ($contacts as $contact)
                <li class="list-group-item">
                    <p>{{ $contact->name }} - {{ $contact->phone }}</p>
                    <form method="POST" action="{{route('addContact')}}">
                        @csrf
                        <input type="hidden" class="form-control" value="{{ $contact->id }}" id="phone_id" name="phone_id">
                        <input type="hidden" class="form-control" value="{{ Auth::user()->id }}" id="user_id" name="user_id">
                        <button type="submit" class="btn btn-primary">Add to contacts</button>
                    </form>
                </li>
            @endforeach
        </ul>
        @endauth
    </main>
@endsection
