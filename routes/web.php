<?php

use Illuminate\Support\Facades\Route;

//Route::get('about', function () {
////    return view('home');
//    return '<H1>ABOUT PAGE</H1>';
//});

Route::get('/', 'HomeController@index')->name('home');

Route::get('/register', 'UserController@create')->name('register.create');
Route::post('/register', 'UserController@store')->name('register.store');

Route::get('/login', 'UserController@loginForm')->name('login.create');
Route::post('/login', 'UserController@login')->name('login');
Route::get('/logout', 'UserController@logout')->name('logout');

Route::get('/cabinet', 'UserController@cabinet')->name('cabinet');
Route::post('/cabinet', 'UserController@addContact')->name('addContact');
Route::get('/cabinet/{id}', 'UserController@deleteContact')->name('deleteContact');
