<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $title = 'Home';
        $contacts = Contact::all();
        return view('home', compact('title', 'contacts'));
    }
}
