<?php

namespace App\Http\Controllers;

use App\User;
use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function create ()
    {
        $title = 'Registration';
        return view('user.create', compact('title'));
    }

    public function store (Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        $request->session()->flash('success', 'You have registered successfully');
        Auth::login($user, true);

        return redirect()->route('home');
    }

    public function loginForm ()
    {
        $title = 'Login';
        return view('user.login', compact('title'));
    }

    public function login (Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
//            return redirect()->route('home');
            return redirect()->route('cabinet');
        }

//        $request->session()->flash('error', 'Incorrect login or password');
        return redirect()->back()->with('error', 'Incorrect login or password');
    }

    public function logout (Request $request)
    {
        Auth::logout();
        return redirect()->route('login.create');
    }

    public function cabinet (Request $request)
    {
        $title = 'Cabinet';
        $user = Auth::user()->id;
        $contacts = User::find($user)->contacts;
        return view('cabinet.index', compact('title', 'contacts'));
    }

    public function addContact (Request $request)
    {
        $phoneID = $request->phone_id;
        $userID = $request->user_id;

        $contactName = Contact::find($phoneID)->name;

        $userContacts = User::find($userID)->contacts;
        $hasContact = false;
        foreach ($userContacts as $userContact) {
            if ($userContact->id == $phoneID) {
                $hasContact = true;
            }
        }

        if (!$hasContact) {
            User::find($userID)->contacts()->attach($phoneID);
            $request->session()->flash('success', 'You have added ' . $contactName . ' to your contacts');
            return redirect()->route('home');
        } else {
            $request->session()->flash('error', 'You already have ' . $contactName . ' in your contacts');
            return redirect()->route('home');
        }
    }

    public function deleteContact (Request $request)
    {
        $phoneID = $request->id;
        $userID = Auth::user()->id;

        $contactName = Contact::find($phoneID)->name;

        User::find($userID)->contacts()->detach($phoneID);
        $request->session()->flash('success', 'You have deleted ' . $contactName . ' from your contacts');
        return redirect()->route('cabinet');
    }
}
